import { Component, OnInit } from '@angular/core';

import { AuthService } from "../../shared/service/auth.service";
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isLoggedIn:boolean = false;
  constructor( private _authService:AuthService) {

  }

  ngOnInit(): void {
     this._authService.isLoggedIn.subscribe((data)=>{
      this.isLoggedIn = data;
    });
  }

  logout(){
    localStorage.removeItem('access_token');
    this._authService.isLoggedIn.next(false);
    this.isLoggedIn = false;
  }

}
