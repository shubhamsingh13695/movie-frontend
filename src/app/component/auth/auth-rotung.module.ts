import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { NonLoggedInGuard } from "../../shared/guards/NonLoggedIn.guard";

const routes: Routes = [
  {
    path:"login",
    component:LoginComponent,
    canActivate:[NonLoggedInGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoutingModule { }
