import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { environment } from "../../../../environments/environment";

@Injectable({
  providedIn:`root`
})

export class LoginService{

  constructor(private httpClient:HttpClient) {

  }


  login(req:LoginReqParams){
    return this.httpClient.post(`${environment.api}/admin/login`,req);
  }


}

export interface LoginReqParams{
  email:string;
  password:string;
}

export interface LoginResponse{
  access_token:string;
  msg:string;
}
