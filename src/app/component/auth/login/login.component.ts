import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../../shared/service/auth.service';

import { LoginService,LoginReqParams, LoginResponse } from "./login.service";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm:FormGroup;
  errorMsg:string;

  constructor(private _fb:FormBuilder,private loginService:LoginService,private router:Router,private auth:AuthService) {
    this.loginForm = this._fb.group({
      email:['',Validators.compose([Validators.required,Validators.email])],
      password:['',Validators.compose([Validators.required,Validators.minLength(5)])]
    });
  }

  ngOnInit(): void {
  }

  getFormField(key){
    return this.loginForm.get(key);
  }

  login(){
    this.errorMsg = null;
    if(this.loginForm.valid){
      let req:LoginReqParams = this.loginForm.value;
      this.loginService.login(req).subscribe((data:LoginResponse)=>{
        localStorage.setItem('access_token',data.access_token);
        this.auth.isLoggedIn.next(true);
        this.router.navigate(['/movie/list']);
      },((errorRes:HttpErrorResponse)=>{
        this.errorMsg = errorRes.error.msg;
      }));

    }
    return false;
  }

}

