import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NonLoggedInGuard } from "../../shared/guards/NonLoggedIn.guard";
import { RoutingModule } from "./auth-rotung.module";

import { LoginComponent } from "./login/login.component";


@NgModule({
  imports:[RoutingModule,CommonModule,FormsModule,ReactiveFormsModule],
  declarations:[LoginComponent],
  providers:[NonLoggedInGuard]
})
export class AuthModule{

}
