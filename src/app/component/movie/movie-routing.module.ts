import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MovieCardComponent } from "./movie-card/movie-card.component";
import { ListMovieComponent } from "./list-movie/list-movie.component";
import { LoggedInGuard } from "../../shared/guards/LoggedIn.guard";
import { AddMovieComponent } from './add-movie/add-movie.component';
const routes: Routes = [
  {
    path:'preview',
    component:MovieCardComponent
  },
  {
    path:'list',
    component:ListMovieComponent,
    canActivate:[LoggedInGuard]
  },
  {
    path:'edit/:id',
    component:AddMovieComponent,
    canActivate:[LoggedInGuard]
  },
  {
    path:'add',
    component:AddMovieComponent,
    canActivate:[LoggedInGuard]
  },
  {
    path:'',
    redirectTo:'preview'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoutingModule { }
