import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
@Injectable({
  providedIn:"root"
})


export class MovieSerivce{


  constructor(private _http:HttpClient) {

  }

  getMovie(req:ListingRequestPrams){
    return this._http.get(`${environment.api}movie/list?page=${req.page}&limit=${req.limit}&search=${req.search}&order=${req.order}&orderBy=${req.orderBy}&genre=${req.genre}`);
  }

  deleteMovie(id:string){
    return this._http.delete(`${environment.api}movie/delete/${id}`);
  }

  saveMovie(data:CreateMovieParams){
    return this._http.post(`${environment.api}movie/create`,data);
  }

  getMovieById(id:string){
    return this._http.get(`${environment.api}movie/show/${id}`);
  }

  updateMovie(id:string,data:UpdateMovieParams){
    return this._http.post(`${environment.api}movie/update/${id}`,data);
  }
}

export class ListingRequestPrams{
  page:number;
  limit:number;
  search?:string = "";
  order?:string = "1";
  orderBy?:string = "name";
  genre?:string="";
}

export interface PaginatedMovie {
  data: Movie[];
  total: number,
  page: number,
  start: number,
  end: number,
  limit: number,
  lastPage: number
}
export interface Movie{
  name:string;
  genre?:any;
  director:any;
  imdb_score:number;
  popularity:number;
  _id:string;
  image?:string;
}

export interface CreateMovieParams{
  name:string;
  genre?:any;
  director:any;
  imdb_score:number;
  popularity:number;
}

export interface UpdateMovieParams{
  genre?:any;
  director:any;
  imdb_score:number;
  popularity:number;
}


export interface UpdateMovieParams{
  genre?:any;
  director:any;
  imdb_score:number;
  popularity:number;
}
