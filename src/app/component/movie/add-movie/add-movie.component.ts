import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GenreService } from "../../../shared/service/genre.service"
import { MovieSerivce } from '../movie.service';
@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.scss']
})
export class AddMovieComponent implements OnInit {

  genereList:any[] = [];

  movieForm:FormGroup;
  id:any;
  constructor(
    private _genreService:GenreService,
    private _formBuilder:FormBuilder,
    private _movieSerivce:MovieSerivce,
    private _router:Router,
    private activatedRoute:ActivatedRoute
  ) {
    this.movieForm = this._formBuilder.group({
      name:['',Validators.compose([Validators.required])],
      director:['',Validators.compose([Validators.required])],
      imdb_score:['',Validators.compose([Validators.required,Validators.required,Validators.min(1),Validators.max(10)])],
      popularity:['',Validators.compose([Validators.required,Validators.min(1),Validators.max(100)])],
      genre:['',Validators.compose([Validators.required])]
    })

  }
  getFormField(key:string){
    return this.movieForm.get(key);
  }
  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params['id'];
    if(this.id){
      this.getFormField('name').disable();
    }
    this.getGenreList();
    this.getMovieById();
  }

  async getGenreList(){
    this.genereList = await this._genreService.getGenre();
  }

  add(){
    if(this.movieForm.valid){
      this._movieSerivce.saveMovie(this.movieForm.value).subscribe((data)=>{
        this._router.navigate(['/movie','list']);
      })
    }
  }

  edit(){
    if(this.movieForm.valid){
      this._movieSerivce.updateMovie(this.id,this.movieForm.value).subscribe((data)=>{
        this._router.navigate(['/movie','list']);
      })
    }
  }

  save(){
    console.log(this.movieForm)
    if(!this.id){
      this.add();
    }
    else{
      this.edit();
    }
  }

  getMovieById(){
    this._movieSerivce.getMovieById(this.id).subscribe((data:any)=>{
      this.movieForm.patchValue(data.data);
    })
  }
}
