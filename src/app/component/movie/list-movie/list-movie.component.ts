import { Component, OnInit } from '@angular/core';
import { faAngleLeft, faAngleRight, faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import { ListingRequestPrams, Movie, MovieSerivce } from "../movie.service";
@Component({
  selector: 'app-list-movie',
  templateUrl: './list-movie.component.html',
  styleUrls: ['./list-movie.component.scss']
})
export class ListMovieComponent implements OnInit {

  page = 1;
  lastPage = 1;
  limit = 10;
  totalMovie:number = 0;
  movieList:Movie[] = [];
  faAngleLeft = faAngleLeft;
  faAngleRight = faAngleRight;
  faEdit = faEdit;
  faTrash = faTrash;

  constructor(
    private _movieService:MovieSerivce,
  ) { }

  ngOnInit(): void {
    this.getMovie(this.page,this.limit);
  }

  getMovie(page,limit){
    let reqParams = new ListingRequestPrams();
    reqParams.page = page;
    reqParams.limit = limit;
    reqParams.orderBy = "createdAt";
    reqParams.order = '-1';
    this._movieService.getMovie(reqParams).subscribe(({data}:any)=>{
      this.lastPage = data.lastPage;
      this.page = data.page;
      this.totalMovie = data.total;
      this.movieList = <Movie[]>data.data;
    })
  }

  delete(id:string,name:string){
    if(confirm(`Are you sure to delete ${name} movie?`)) {
      this._movieService.deleteMovie(id).subscribe((data)=>{
        this.getMovie(this.page,this.limit);
      });
    }
  }

}
