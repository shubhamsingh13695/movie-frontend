import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { ListingRequestPrams, Movie, MovieSerivce } from "../movie.service";

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.scss']
})
export class MovieCardComponent implements OnInit {

  faStar  = faStar;
  page = 0;
  lastPage = 1;
  limit = 16;
  searchString:string = "";
  movieList:Movie[] = [];
  apiExceute:boolean = false;
  order:string = "1";
  orderBy:string = "name";
  genre:string = "";

  constructor(
    private _movieService:MovieSerivce,
    private activatedRouter:ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.activatedRouter.queryParamMap.subscribe((data)=>{
      this.searchString = data.get('search') ? data.get('search') : '';
      this.order = data.get('order') ? data.get('order') : '1';
      this.orderBy = data.get('orderBy') ? data.get('orderBy') : 'name';
      this.genre = data.get('genre') ? data.get('genre') : '';
      this.resetData();
      this.getMovie();
    });
  }

  getMovie(){
    this.apiExceute = true;
    let reqParams:ListingRequestPrams = {
      page:this.page+1,
      limit:this.limit,
      search:this.searchString,
      order:this.order,
      orderBy:this.orderBy,
      genre:this.genre
    }
    this._movieService.getMovie(reqParams).subscribe(({data}:any)=>{
      this.apiExceute = false;
      this.lastPage = data.lastPage;
      this.page = data.page;
      this.movieList.push(...appendImageToObject(<Movie[]>data.data));
    })
  }

  resetData(){
    this.page = 0;
    this.lastPage = 1;
    this.limit = 16;
    this.movieList = [];
  }

}


function appendImageToObject(data:Movie[]){
  data = data.map((movie:Movie)=>{
    let min = 500;
    let max = 1000;
    let height = Math.floor(Math.random() * (max - min + 1)) + min;
    let width = Math.floor(Math.random() * (max - min + 1)) + min;
    return { ...movie,image:`https://source.unsplash.com/${height}x${width}`};
  });
  return data;
}
