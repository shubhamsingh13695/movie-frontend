import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RoutingModule } from "./movie-routing.module";
import { MovieCardComponent } from "./movie-card/movie-card.component";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SharedModule } from "../../shared/shared.module";
import { ListMovieComponent } from "./list-movie/list-movie.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LoggedInGuard } from "../../shared/guards/LoggedIn.guard";
import { AddMovieComponent } from './add-movie/add-movie.component';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    FontAwesomeModule,
    SharedModule,
    FormsModule,
    NgSelectModule,
    ReactiveFormsModule
  ],
  declarations: [
    MovieCardComponent,
    ListMovieComponent,
    AddMovieComponent
  ],
  providers:[
    LoggedInGuard
  ]
})

export class MovieModule {

}
