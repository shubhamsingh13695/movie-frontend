import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path:'movie',
    loadChildren:()=>import("./component/movie/movie.module").then(_=> _.MovieModule)
  },
  {
    path:'auth',
    loadChildren:()=>import("./component/auth/auth.module").then(_=> _.AuthModule)
  },
  {
    path:'',
    redirectTo:'movie',
    pathMatch:"full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
