import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn:"root"
})

export class GenreService{

  genre:Genre[] = [];
  constructor(private http:HttpClient){

  }

  async getGenre(){
    if (this.genre.length){
      return this.genre;
    }
    else{

      let _:any = await this.http.get(`${environment.api}genre/list`).toPromise();
      this.genre = <Genre[]>(_.data);
      return this.genre;
    }
  }

}


export interface Genre{
  name:string;
  _id:string;
}
