import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn:`root`
})

export class AuthService{

  isLoggedIn = new BehaviorSubject<boolean>(false);
  constructor(private httpClient:HttpClient) {

    if(localStorage.getItem('access_token')){
      this.isLoggedIn.next(true);
    }

  }


}

