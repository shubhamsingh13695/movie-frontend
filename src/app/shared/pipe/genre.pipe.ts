import { Pipe, PipeTransform } from '@angular/core';
@Pipe({name: 'genre'})
export class GenrePipe implements PipeTransform {

  transform(value: any[]): string {
    let genre = [];
    let genreList = value.splice(0,4);
    if (!genreList.length){
      return "No genre Found!."
    }
    genre = genreList.map((_el)=>{
      return _el.name;
    })

    return genre.join(",");
  }

}
