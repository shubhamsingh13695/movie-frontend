import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { GenreService,Genre } from "../service/genre.service";
import { sortBy } from "./constant";

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  faSearch = faSearch;
  genreList:Genre[] = [];

  _searchText:string="";
  sortList = sortBy;

  filters:any[] = [];

  @Output() searchText = new EventEmitter();

  constructor(
    private _genreService:GenreService,
    private _router:Router,
    private activatedRouter:ActivatedRoute
  ) { }

  ngOnInit(): void {
    this._searchText = this.activatedRouter.snapshot.queryParamMap.get('search');
    this.filters  = this.activatedRouter.snapshot.queryParamMap.get('genre') ? this.activatedRouter.snapshot.queryParamMap.get('genre').split(','):[];
    this.getGenre();
  }


  async getGenre(){
    this.genreList = await this._genreService.getGenre();
  }

  textSearch(){
    this._router.navigate([],{queryParams:{search:this._searchText},queryParamsHandling: "merge"})
  }
  selectSort({target:{value}}){
    this._router.navigate([],{queryParams:{order:this.sortList[value]['order'],orderBy:this.sortList[value]['orderBy']},queryParamsHandling: "merge"})
  }

  appendFilter(value){
    if(this.filters.includes(value)){
      this.filters = this.filters.filter((el)=>el!==value);
    }
    else{
      this.filters.push(value);
    }
    this._router.navigate([],{queryParams:{genre:this.filters.join(",")},queryParamsHandling: "merge"})
  }

  isChecked(id){
    return this.filters.includes(id);
  }
  showAll(){
    this.filters = [];
    this._router.navigate([],{queryParams:{genre:""},queryParamsHandling: "merge"})
  }
}
