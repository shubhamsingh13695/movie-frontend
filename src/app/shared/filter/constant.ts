export const sortBy = [
  {name:"Sort by Movie name ASC",orderBy:'name',order:'1'},
  {name:"Sort by Movie name DESC",orderBy:'name',order:'-1'},
  {name:"Sort by Director name ASC",orderBy:'director',order:'1'},
  {name:"Sort by Director name DESC",orderBy:'director',order:'-1'},
  {name:"Sort by IMDB rating ASC",orderBy:'imdb_score',order:'1'},
  {name:"Sort by IMDB rating DESC",orderBy:'imdb_score',order:'-1'},
  {name:"Sort by 99Popularity ASC",orderBy:'popularity',order:'1'},
  {name:"Sort by 99Popularity DESC",orderBy:'popularity',order:'-1'}
]
