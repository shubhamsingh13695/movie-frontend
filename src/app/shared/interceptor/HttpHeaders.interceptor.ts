import { Injectable } from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';

@Injectable()
export class HttpHeadersInterceptor implements HttpInterceptor {

    constructor(
      private _router: Router,
      private authService: AuthService
    ) { }


    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>
    {
        let headers: HttpHeaders = request.headers;
        const token = localStorage.getItem('access_token');
        if(token){
            headers = headers.append('Authorization', 'Bearer ' + token);
        }
        request = request.clone({
            headers: headers
        });
        return next.handle(request).pipe(catchError((error: HttpErrorResponse) => {
          switch (error.status) {
            case 401:
              this.authService.isLoggedIn.next(false);
              this._router.navigate(['/auth/login']);
              break;
            case 403:
              this.authService.isLoggedIn.next(false);
              this._router.navigate(['/auth/login']);
              break;
          }
          return throwError(error);
      }));
    }
}
