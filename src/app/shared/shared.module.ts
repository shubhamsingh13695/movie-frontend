import { NgModule } from "@angular/core";
import { FilterComponent } from "./filter/filter.component";
import { GenrePipe } from "./pipe/genre.pipe";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CommonModule } from "@angular/common";
import { FormsModule } from '@angular/forms';

@NgModule({
  imports:[FontAwesomeModule,CommonModule,FormsModule],
  declarations:[FilterComponent,GenrePipe],
  exports:[FilterComponent,GenrePipe],
})

export class SharedModule{

}


